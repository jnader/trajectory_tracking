close all 
clc

%% LOG FILES TREATMENT SCRIPT

% To fill


% We need to extract :

% q = [x y z vx vy vz qw qx qy qz wx wy wz]
% qd = [xd yd zd vxd vyd vzd qwd qxd qyd qzd wxd wyd wzd]
% u = [u1 u2 u3 u4]

% Then we compute :

% Final/integral state error norm 
% Integral input error norm

%% Loading the log files :

firstimport = 1; % 1 if you want to import, 0 otherwise


if firstimport == 1
    
    %path = '/home/scalp/Documents/work/experiments/trajectory_tracking/save_logs';
    %cd(path)
    
    %newdir = [datestr(now,30) '_repetability'];
    %mkdir(newdir);
    %newpath = [path '/' newdir];

    %system(['mv /tmp/mikro*.log /tmp/nhfc*.log /tmp/pom*.log ' newpath]);
    %cd(newdir);
    delete('pom_meas*.log');
    %delete('pom.log'); delete('mikro.log'); delete('nhfc.log');

    %system('sed -s -i ''1,36d'' mikro*.log');
    system('sed -s -i ''1,1d'' *.log');
end

%system('sed -s -i ''1,1d'' *.log');
                        
nhfc_logs = dir('nhfc*.log');
pom_logs = dir('pom*.log');
mikro_logs = dir('mikro*.log');

nhfc_data = {};
pom_data = {};
mikro_data = {};

tic
for i=1:length(nhfc_logs)
    nhfc_data{i} = dlmread(nhfc_logs(i).name);
    pom_data{i} = dlmread(pom_logs(i).name);
    mikro_data{i} = dlmread(mikro_logs(i).name);
end
toc

%% Extracting columns of interest and reorganizing

% nhfc_needed_columns = [ts xd yd zd rolld pitchd yawd vxd vyd vzd wxd wyd wzd] 1, 14:25
% pom_needed_columns = [ts x y z roll pitch yaw vx vy vz wx wy wz] 1, 8:19
% mikro_needed_columns = [ts cmd_v0 cmd_v1 cmd_v2 cmd_v3] 1, 14:17

nhfc = {};
pom = {};
mikro = {};

for i=1:length(nhfc_logs)
    nhfc{i} = [nhfc_data{i}(:, 1) nhfc_data{i}(:, 14:16) nhfc_data{i}(:, 20:22) eul2quat(nhfc_data{i}(:, 17:19)) nhfc_data{i}(:, 23:25)];
    pom{i} = [pom_data{i}(:, 1) pom_data{i}(:, 8:10) pom_data{i}(:, 14:16) eul2quat(pom_data{i}(:, 11:13)) pom_data{i}(:, 17:19)];
    mikro{i} = [mikro_data{i}(:, 1) mikro_data{i}(:, 22:25)];
end

%clear nhfc_logs pom_logs mikro_logs nhfc_data pom_data mikro_data

% nhfc_needed_columns = [ts xd yd zd vxd vyd vzd qwd qxd qyd qzd wxd wyd wzd]
% pom_needed_columns = [ts x y z vx vy vz qw qx qy qz wx wy wz]
% mikro_needed_columns = [ts cmd_v0 cmd_v1 cmd_v2 cmd_v3]

%% Removing time offset

% TODO : check synchronization of each trajectory (desired value needs to
% be synced)

for i=1:length(nhfc)
    it = max([nhfc{i}(1, 1) pom{i}(1, 1) mikro{i}(1, 1)]);
    nhfc{i}(:, 1) = nhfc{i}(:, 1) - it*ones(size(nhfc{i}(:, 1)));
    pom{i}(:, 1) = pom{i}(:, 1) - it*ones(size(pom{i}(:, 1)));
    mikro{i}(:, 1) = mikro{i}(:, 1) - it*ones(size(mikro{i}(:, 1)));
end

for i=1:length(pom)
    pom{i}(:, 2:4) = pom{i}(:, 2:4) - pom{i}(1, 2:4).*ones(size(pom{i}(:, 2:4)));
    nhfc{i}(:, 2:4) = nhfc{i}(:, 2:4) - nhfc{i}(1, 2:4).*ones(size(nhfc{i}(:, 2:4)));
end

%% Plot different curves

titlepos = {'x', 'y', 'z', 'vx', 'vy', 'vz'};
titleatt = {'qw', 'qx', 'qy', 'qz', 'wx', 'wy', 'wz'};
titlein = {'u1', 'u2', 'u3', 'u4'};

% Position display

t = pom{1}(:, 1);

figure;
set(gcf,'Units','centimeters','Position',[1 2 48 22],'Color', 'w')

for i = 1:length(titlepos)
    subplot(2,3,i);
    trajs_plot(nhfc, i+1)
    trajs_plot(pom, i+1)
    title(titlepos(i))
end

% Attitude display

figure;
set(gcf,'Units','centimeters','Position',[1 2 48 22],'Color', 'w');

for i = 1:length(titleatt)
    if i < 5
        subplot(2,4,i);
    else
        subplot(2,4,i+1);
    end
    trajs_plot(nhfc, i+7)
    trajs_plot(pom, i+7)
    title(titleatt(i))
end

% Inputs display

figure;
set(gcf,'Units','centimeters','Position',[1 2 36 12],'Color', 'w');

for i = 1:length(titlein)
    subplot(1,4,i);
    trajs_plot(mikro, i+1)
    axis([0 5 0 100])
    title(titlein(i))
end

% Trajectory display

% opt_cond = 'IG';
% traj_num = 1; % !! don't choose 2
% 
% % TODO : find another way to select the variable name (eval is not
% % recommended)
% 
% wp = []; wp_t = eval(['traj_' num2str(traj_num) '.' opt_cond '_wp_t']);
% for i = 1:length(wp_t)
%     wp_tmp = eval(['traj_' num2str(traj_num) '.' opt_cond '_wp']);
%     wp = [wp squeeze(wp_tmp(i, :, :))];
% end
% 
% xyzyaw_t = zeros(4, length(t));
% 
% for i = 1:length(t)
%     xyzyaw_t = [xyzyaw_t waypoints2trajectory(t(i), wp, wp_t)'];
% end

figure;
set(gcf,'Units','centimeters','Position',[1 2 24 18],'Color', 'w');

plot3(xyzyaw_t(1, :), xyzyaw_t(2, :), xyzyaw_t(3, :), 'LineWidth', 2, 'Color', 'k')
hold on
for i=1:length(pom)
    plot3(pom{i}(:, 2), pom{i}(:, 3), pom{i}(:, 4), 'LineWidth', 2, 'Color', [1 i/length(pom) 0])
    hold on
end
grid on
axis equal

xlabel('X','FontSize',16) % description de la grandeur des abscisses (+ exemple code LaTeX)
ylabel('Y','FontSize',16)
zlabel('Z','FontSize',16)

disp('done')


%% FUNCTIONS

function trajs_plot(data, col)
    n = length(data);
    if n > 1
        for i = 1:n
            if (0<i) && (i<n/2)
                plot(data{i}(:, 1), data{i}(:, col), 'Color', [1 2*i/n 0])
                hold on
            else
                plot(data{i}(:, 1), data{i}(:, col), 'Color',[1-(i-1)/n i/n 1/n]) 
                hold on
            end
        end
    else
        plot(data{1}(:, 1), data{1}(:, col), 'Color','k')
    end
    grid on
end





