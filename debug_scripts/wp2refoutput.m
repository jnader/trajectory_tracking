function [pos, vel, acc] = waypoints2trajectory(wp_t, wp, t)

    % here we assume that N_jc = 3 (pos, vel, acc for linear constraints)
    % and N_outputs = 4 (x, y, z, yaw)

    N_jc = 3;
    N_outputs = 4;
    N_coeff = 2*N_jc; % degree + 1 of each Bézier curve

    if t ~= wp_t(end)
        % select the right piece of the trajectory (between 2 waypoints)

        select = 1; % piece number selector
        while wp_t(select+1) <= t
            select = select+1;
        end
                
        wp_i = wp(:, N_outputs*(select-1)+1:N_outputs*select);
        wp_f = wp(:, N_outputs*(select)+1:N_outputs*(select+1));
        DT = wp_t(select+1) - wp_t(select);
        s = (t - wp_t(select))/DT;

        b = zeros(2*N_jc*N_outputs, 1); % to store limit conditions

        % limit conditions

        for i = 1:N_outputs
            for j = 1:N_jc
                b(2*(N_jc*(i - 1) + (j-1)) + 1) = wp_i(j, i);
                b(2*(N_jc*(i - 1) + (j-1)) + 2) = wp_f(j, i); 
            end
        end

        % constraints matrix

        M = [1 zeros(1, N_coeff-1);                          % polynomial constraints for ti
             zeros(1, N_coeff-1) 1;                          % for tf
            -N_coeff/DT N_coeff/DT zeros(1, N_coeff-2);                        % polynomial derivative for ti
            zeros(1, N_coeff-2) -N_coeff/DT N_coeff/DT;                        % for tf
            N_coeff*(N_coeff-1)/DT^2 -2*N_coeff*(N_coeff-1)/DT^2 N_coeff*(N_coeff-1)/DT^2 zeros(1,N_coeff-3);  % second order derivative for ti
            zeros(1, N_coeff-3) N_coeff*(N_coeff-1)/DT^2 -2*N_coeff*(N_coeff-1)/DT^2 N_coeff*(N_coeff-1)/DT^2;]; % for tf


        M = blkdiag(M, M, M, M); % 4 coords
        M_inv = pinv(M);

        a = M_inv*b; % control points of the piece

        bp_rd = zeros(N_coeff, 1);
        bp_vd = zeros(N_coeff, 1);
        bp_ad = zeros(N_coeff, 1);
        traj_build = zeros(N_outputs, N_coeff);

        for i = 1:N_outputs
            traj_build(i, :) = a(1+(i-1)*N_coeff:i*N_coeff);
        end

        for i=0:N_coeff-1
            bp_rd(i+1) = s^i*(1-s)^(N_coeff-1-i)*nchoosek(N_coeff-1,i);
            bp_vd(i+1) = (i*s^(i-1)*(1-s)^(N_coeff-1-i) - (N_coeff-1-i)*(s)^i*(1-s)^(N_coeff-i-2))*nchoosek(N_coeff-1,i)/DT;
            bp_ad(i+1) = (i*(i-1)*s^(i-2)*(1-s)^(N_coeff-1-i) - 2*i*(N_coeff-1-i)*s^(i-1)*(1-s)^(N_coeff-i-2) + (N_coeff-1-i)*(N_coeff-i-2)*s^i*(1-s)^(N_coeff-i-3))*nchoosek(N_coeff-1,i)/DT^2;
        end

        pos = bp_rd'*traj_build'; % position
        vel = bp_vd'*traj_build'; % velocity
        acc = bp_ad'*traj_build'; % acceleration
    else
        % if last point of the trajectory
        pos = wp(1, end-N_outputs+1:end); % position
        vel = wp(2, end-N_outputs+1:end); % velocity
        acc = wp(3, end-N_outputs+1:end); % acceleration
    end
    N_jc = 3;
    N_outputs = 4;
    N_coeff = 2*N_jc; % degree + 1 of each Bézier curve

    if t ~= wp_t(end)
        % select the right piece of the trajectory (between 2 waypoints)

        select = 1; % piece number selector
        while wp_t(select+1) <= t
            select = select+1;
        end

        wp_i = wp(:, N_outputs*(select-1)+1:N_outputs*select);
        wp_f = wp(:, N_outputs*(select)+1:N_outputs*(select+1));
        DT = wp_t(select+1) - wp_t(select);
        s = (t - wp_t(select))/DT;

        b = zeros(2*N_jc*N_outputs, 1); % to store limit conditions

        % limit conditions

        for i = 1:N_outputs
            for j = 1:N_jc
                b(2*(N_jc*(i - 1) + (j-1)) + 1) = wp_i(j, i);
                b(2*(N_jc*(i - 1) + (j-1)) + 2) = wp_f(j, i); 
            end
        end

        % constraints matrix

        M = [1 zeros(1, N_coeff-1);                          % polynomial constraints for ti
             zeros(1, N_coeff-1) 1;                          % for tf
            -N_coeff/DT N_coeff/DT zeros(1, N_coeff-2);                        % polynomial derivative for ti
            zeros(1, N_coeff-2) -N_coeff/DT N_coeff/DT;                        % for tf
            N_coeff*(N_coeff-1)/DT^2 -2*N_coeff*(N_coeff-1)/DT^2 N_coeff*(N_coeff-1)/DT^2 zeros(1,N_coeff-3);  % second order derivative for ti
            zeros(1, N_coeff-3) N_coeff*(N_coeff-1)/DT^2 -2*N_coeff*(N_coeff-1)/DT^2 N_coeff*(N_coeff-1)/DT^2;]; % for tf


        M = blkdiag(M, M, M, M); % 4 coords
        M_inv = pinv(M);

        a = M_inv*b; % control points of the piece

        bp_rd = zeros(N_coeff, 1);
        bp_vd = zeros(N_coeff, 1);
        bp_ad = zeros(N_coeff, 1);
        traj_build = zeros(N_outputs, N_coeff);

        for i = 1:N_outputs
            traj_build(i, :) = a(1+(i-1)*N_coeff:i*N_coeff);
        end

        for i=0:N_coeff-1
            bp_rd(i+1) = s^i*(1-s)^(N_coeff-1-i)*nchoosek(N_coeff-1,i);
            bp_vd(i+1) = (i*s^(i-1)*(1-s)^(N_coeff-1-i) - (N_coeff-1-i)*(s)^i*(1-s)^(N_coeff-i-2))*nchoosek(N_coeff-1,i)/DT;
            bp_ad(i+1) = (i*(i-1)*s^(i-2)*(1-s)^(N_coeff-1-i) - 2*i*(N_coeff-1-i)*s^(i-1)*(1-s)^(N_coeff-i-2) + (N_coeff-1-i)*(N_coeff-i-2)*s^i*(1-s)^(N_coeff-i-3))*nchoosek(N_coeff-1,i)/DT^2;
        end

        pos = bp_rd'*traj_build'; % position
        vel = bp_vd'*traj_build'; % velocity
        acc = bp_ad'*traj_build'; % acceleration
    else
        % if last point of the trajectory
        pos = wp(1, end-N_outputs+1:end); % position
        vel = wp(2, end-N_outputs+1:end); % velocity
        acc = wp(3, end-N_outputs+1:end); % acceleration
    end
end