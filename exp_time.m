function [wp_out, t_out, exp_out, state_out] = exp_time(wp_t, wp_in, t_in, quad_pos, start_traj, max_traj, ts)
   

    %% Manage all the runs for 1 flight of closed-loop sensitivity experiments
    
    coder.extrinsic('run');
    
    % Settings and init :
    
    cutidx = floor(size(wp_in, 2)/2);  % cut wp at the middle because it contains both directions (left & right)
    endpause = 1;  % pause time after a trajectory
    split_logs = 0;  % default 1, put at 0 when trying to debug

    persistent ref_t  % store reference time when starting a trajectory
    persistent nb_times  % count number of done runs (should always be less than/equal to max_traj)
    persistent wp_run  % store waypoints during run
    persistent last_was_switch  % store if there was a state switch or not
    persistent pauseval  % value to store current pause after trajectory
    persistent exp_state
   
    
    % exp_state :  0  init (converge to left origin)
    %              1  trajectory to the right
    %              2  go to right origin
    %              3  trajectory to the left
    %              4  go to left origin
    %              5  land after all runs
    
    wp_out = zeros(size(wp_in, 1), cutidx);
    t_out = 0;
    exp_out = 0 > 0.5;
    state_out = 0;
    
    % init when no time reference has already been saved: 
    if isempty(ref_t)
        ref_t = 0;
        exp_out = 0 > 0.5;  % false
        exp_state = 0;
        last_was_switch = 1;
        nb_times = 0;
        wp_run = zeros(size(wp_in, 1), cutidx);  % define wp with cut
        wp_out = wp_run; 
        pauseval = 0;
        
        if split_logs == 0
            run start_logs_script.m
        end
    end
    
    %% exp_state = 0 --> 0, 1: init (converge to left origin)
    
    if (exp_state == 0) 
        
        if last_was_switch == 1
            run reset_nhfc_left.m  % go to left origin before first run
        end
        
        if start_traj == 1  % next state
            
            exp = 1 > 0.5;  % True
            exp_state = 1;  % trajectory to the right
            last_was_switch = 1;
        
            ref_t = t_in;  % store reference time
        
            % Get current position and yaw to compute left origin:
            eul_angles = quat2eul([quad_pos(4), quad_pos(5), quad_pos(6), quad_pos(7)]);
            yaw = eul_angles(1);
            origin = [quad_pos(1), quad_pos(2), quad_pos(3), yaw];
        
            wp = wp_in(:, 1:cutidx);  % cut wp -> traj to the right
            wp(1, :) = wp(1, :) + repmat(origin, 1, length(wp_t));
            wp_run = wp;  % save wp for run
            
            run test_set_geom.m;  % parameters: nominal or perturbed
            
            if split_logs == 1
                run start_logs_script.m;
            end
     
        else
            exp = 0 > 0.5;  % stay False
            exp_state = 0;  % convergence not done yet, stay here
            last_was_switch = 0;
            wp = zeros(size(wp_in, 1), cutidx);
            wp_run = wp;
        end
        
        wp_out = wp;
        t_out = 0;
        exp_out = exp;
        state_out = exp_state;
        
    %% exp_state = 1 --> 1, 2, 5: trajectory to the right
    
    elseif (exp_state == 1)
        
        t_traj = t_in - ref_t;  % update trajectory time
        
        if t_traj >= wp_t(end)  % trajectory finished
            
            if pauseval*ts < endpause  % pause
                
                exp = 1 > 0.5;  % keep exp True
                exp_state = 1;  % stay here for a bit
                t_traj = wp_t(end);
                last_was_switch = 0;
                wp = wp_run;
                
                if pauseval == 0
                    run reset_set_geom.m;  % reset parameters to nominal
                    
                    if split_logs == 1
                        run stop_logs_script.m; 
                    end
                end
                
                pauseval = pauseval + 1;
            else
                exp = 0 > 0.5;  % False
                exp_state = 2;
                last_was_switch = 1;
                t_traj = wp_t(end);
                ref_t = 0;
                wp = zeros(size(wp_in, 1), cutidx); 

                nb_times = nb_times + 1;  % increment number of finished trajectories
                pauseval = 0;

                if nb_times >= max_traj
                    exp_state = 5;  % go land afer experiments
                    last_was_switch = 1;
                end
            end
        else
            exp = 1 > 0.5;  % stay True
            exp_state = 1;  % trajectory not finished yet
            last_was_switch = 0;
            wp = wp_run;  % use stored value for the run
        end
        
       wp_out = wp;
       t_out = t_traj;
       exp_out = exp;
       state_out = exp_state;
       
       
    %% exp_state = 2 --> 2, 3: converge to right origin
    
    elseif (exp_state == 2) 
        
        if last_was_switch == 1
            run reset_nhfc_right.m  % go to right origin after trajectory to the right
        end
        
        if (start_traj == 1) && (nb_times < max_traj) % next state
            
            exp = 1 > 0.5;  % True
            exp_state = 3;  % trajectory to the left
            last_was_switch = 1;
        
            ref_t = t_in;  % store reference time
        
            % Get current position and yaw to compute left origin:
            eul_angles = quat2eul([quad_pos(4), quad_pos(5), quad_pos(6), quad_pos(7)]);
            yaw = eul_angles(1);
            origin = [quad_pos(1), quad_pos(2), quad_pos(3), yaw];
        
            wp = wp_in(:, cutidx+1:end);  % cut wp -> traj to the left
            wp(1, :) = wp(1, :) + repmat(origin, 1, length(wp_t));
            wp_run = wp;  % save wp for run
            
            run test_set_geom.m;  % parameters: nominal or perturbed
            
            if split_logs == 1
                run start_logs_script.m;
            end
     
        else
            exp = 0 > 0.5;  % stay False
            exp_state = 2;  % convergence not done yet, stay here
            last_was_switch = 0;
            wp = zeros(size(wp_in, 1), cutidx);
            wp_run = wp;
        end
        
        wp_out = wp;
        t_out = 0;
        exp_out = exp;
        state_out = exp_state;
        
    %% exp_state = 3 --> 3, 4, 5: trajectory to the left
    
    elseif (exp_state == 3)
        
        t_traj = t_in - ref_t;  % update trajectory time
        
        if t_traj >= wp_t(end)  % trajectory finished
            
            if pauseval*ts < endpause  % pause
                
                exp = 1 > 0.5;  % keep exp True
                exp_state = 3;  % stay here for a bit
                t_traj = wp_t(end);
                last_was_switch = 0;
                wp = wp_run;
                
                if pauseval == 0
                    run reset_set_geom.m;  % reset parameters to nominal
                    
                    if split_logs == 1
                        run stop_logs_script.m; 
                    end
                end
                
                pauseval = pauseval + 1;
            else
                exp = 0 > 0.5;  % False
                exp_state = 4;
                last_was_switch = 1;
                t_traj = wp_t(end);
                ref_t = 0;
                wp = zeros(size(wp_in, 1), cutidx); 

                nb_times = nb_times + 1;  % increment number of finished trajectories
                pauseval = 0;

                if nb_times >= max_traj
                    exp_state = 5;  % go land afer experiments
                    last_was_switch = 1;
                end
            end
        else
            exp = 1 > 0.5;  % stay True
            exp_state = 3;  % trajectory not finished yet
            last_was_switch = 0;
            wp = wp_run;  % use stored value for the run
        end         
        
       wp_out = wp;
       t_out = t_traj;
       exp_out = exp;
       state_out = exp_state;
       
    %% exp_state = 4 --> 4, 1: converge to left origin
    
    elseif (exp_state == 4) 
        
        if last_was_switch == 1
            run reset_nhfc_left.m  % go to left origin after trajectory to the left
        end
        
        if start_traj == 1  % next state
            
            exp = 1 > 0.5;  % True
            exp_state = 1;  % trajectory to the right
            last_was_switch = 1;
        
            ref_t = t_in;  % store reference time
        
            % Get current position and yaw to compute left origin:
            eul_angles = quat2eul([quad_pos(4), quad_pos(5), quad_pos(6), quad_pos(7)]);
            yaw = eul_angles(1);
            origin = [quad_pos(1), quad_pos(2), quad_pos(3), yaw];
        
            wp = wp_in(:, 1:cutidx);  % cut wp -> traj to the right
            wp(1, :) = wp(1, :) + repmat(origin, 1, length(wp_t));
            wp_run = wp;  % save wp for run
            
            run test_set_geom.m;  % parameters: nominal or perturbed
            
            if split_logs == 1
                run start_logs_script.m; 
            end
     
        else
            exp = 0 > 0.5;  % stay False
            exp_state = 4;  % convergence not done yet, stay here
            last_was_switch = 0;
            wp = zeros(size(wp_in, 1), cutidx);
        end
        
        wp_out = wp;
        t_out = 0;
        exp_out = exp;
        state_out = exp_state;
           
        
    %% exp_state = 5: land after all runs
    
    elseif (exp_state == 5)
        
        if last_was_switch == 1
            run land_after_exp.m
        end
        
        wp_out = zeros(size(wp_in, 1), cutidx);
        t_out = 0;
        exp_out = 0 > 0.5;  % False
        state_out = exp_state;
        
        if split_logs == 0
            run stop_logs_script.m; 
        end
    end