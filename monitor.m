close all

%% get variables from Simulink

% Times:
tsim = tref_sim.time;
tref = tref_sim.signals.values;

% Experiment state, strat_traj:
expstate = expstate_sim.signals.values;

% Waypoints and time of waypoints:
wp = squeeze(wp_sim.signals.values);
wp_t = squeeze(wp_t_sim.signals.values);

% Desired position, speed and acceleration:
posref = squeeze(posref_sim.signals.values);
velref = squeeze(velref_sim.signals.values);
accref = squeeze(accref_sim.signals.values);
xref = posref(1, :); vxref = velref(1, :); axref = velref(1, :);
yref = posref(2, :); vyref = velref(1, :); ayref = velref(1, :);
zref = posref(3, :); vzref = velref(1, :); azref = velref(1, :);
yawref = posref(4, :); vyawref = velref(1, :); ayawref = velref(1, :);

% Quadrotor state:
posquat = squeeze(posquat_sim.signals.values);
vel = squeeze(vel_sim.signals.values);
%acc = squeeze(acc_sim.signals.values);
dt = tsim(2) - tsim(1);
x = posquat(:, 1); vx = vel(:, 1); %ax = acc(:, 1);
y = posquat(:, 2); vy = vel(:, 2); %ay = acc(:, 2);
z = posquat(:, 3); vz = vel(:, 3); %az = acc(:, 3);
quat = posquat(:, 4:7);
yaw = zeros(1, length(tsim));
for i = 1:size(quat, 1)
    current_eul = quat2eul(quat(i, :));
    yaw(i) = current_eul(1);
end
vyaw = diff(yaw)/dt; ayaw = diff(vyaw)/dt;


idxs = 1; idxe = length(tsim);
%index = find(tsim==put val here);
%idxs = index - 1; idxe = index + 1;

pathlogs = '/home/scalp/Documents/github/cls-savelogs/';

if pcase == 0
    save([pathlogs 'traj_' num2str(traj_num) '/' opt_cond '/nom/' datestr(now, 30) '.mat'], ...
        'systime', 'tsim', 'tref', 'expstate', 'wp', 'wp_t', 'posref', 'velref', 'accref', 'posquat', 'vel');
else
    save([pathlogs 'traj_' num2str(traj_num) '/' opt_cond '/per_f' num2str(flight_num) '/' datestr(now, 30) '.mat'], ...
        'systime', 'tsim', 'tref', 'expstate', 'wp', 'wp_t', 'posref', 'velref', 'accref', 'posquat', 'vel');
end

disp('Saved .mat file, save logs from terminal !');

plots = 0;

%% Plots

if plots == 1

    % State of experiment figure
    figure;
    set(gcf,'Units','centimeters','Position',[1 2 32 18], 'Color', 'w');
    plot(tsim(idxs:idxe), tref(idxs:idxe))
    legend('t_{ref}')
    grid on

    % State of experiment figure
    figure;
    set(gcf,'Units','centimeters','Position',[1 2 32 18], 'Color', 'w');
    plot(tsim(idxs:idxe), expstate(idxs:idxe), 'r')
    hold on
    legend('exp_{state}')
    grid on

    % Output pos

    figure;
    set(gcf,'Units','centimeters','Position',[1 2 32 18], 'Color', 'w');

    subplot(2, 2, 1)
    plot(tsim(idxs:idxe), x(idxs:idxe))
    hold on
    plot(tsim(idxs:idxe), xref(idxs:idxe), 'k')
    legend('x', 'x_{ref} [m]')
    grid on

    subplot(2, 2, 2)
    plot(tsim(idxs:idxe), y(idxs:idxe))
    hold on
    plot(tsim(idxs:idxe), yref(idxs:idxe), 'k')
    legend('y','y_{ref} [m]')
    grid on

    subplot(2, 2, 3)
    plot(tsim(idxs:idxe), z(idxs:idxe))
    hold on
    plot(tsim(idxs:idxe), zref(idxs:idxe), 'k')
    legend('z', 'z_{ref} [m]')
    grid on

    subplot(2, 2, 4)
    plot(tsim(idxs:idxe), yaw(idxs:idxe))
    hold on
    plot(tsim(idxs:idxe), yawref(idxs:idxe), 'k')
    legend('\psi', '\psi_{ref} [rad]')
    grid on
    
    % Output velocity

    figure;
    set(gcf,'Units','centimeters','Position',[1 2 32 18], 'Color', 'w');

    subplot(2, 2, 1)
    plot(tsim(idxs:idxe), vx(idxs:idxe))
    hold on
    plot(tsim(idxs:idxe), vxref(idxs:idxe), 'k')
    legend('v_x', 'ref [m/s]')
    grid on

    subplot(2, 2, 2)
    plot(tsim(idxs:idxe), vy(idxs:idxe))
    hold on
    plot(tsim(idxs:idxe), vyref(idxs:idxe), 'k')
    legend('v_y','ref [m/s]')
    grid on

    subplot(2, 2, 3)
    plot(tsim(idxs:idxe), vz(idxs:idxe))
    hold on
    plot(tsim(idxs:idxe), vzref(idxs:idxe), 'k')
    legend('v_z', 'ref [m/s]')
    grid on

    subplot(2, 2, 4)
    plot(tsim(idxs:idxe-1), vyaw(idxs:idxe-1))
    hold on
    plot(tsim(idxs:idxe), vyawref(idxs:idxe), 'k')
    legend('v_{\psi}', 'ref [rad/s]')
    grid on
    
    % Output acceleration

%     figure;
%     set(gcf,'Units','centimeters','Position',[1 2 32 18], 'Color', 'w');
% 
%     subplot(2, 2, 1)
%     plot(tsim(idxs:idxe), ax(idxs:idxe))
%     hold on
%     plot(tsim(idxs:idxe), axref(idxs:idxe), 'k')
%     legend('a_x', 'ref [m/s^2]')
%     grid on
% 
%     subplot(2, 2, 2)
%     plot(tsim(idxs:idxe), ay(idxs:idxe))
%     hold on
%     plot(tsim(idxs:idxe), ayref(idxs:idxe), 'k')
%     legend('a_y','ref [m/s^2]')
%     grid on
% 
%     subplot(2, 2, 3)
%     plot(tsim(idxs:idxe), az(idxs:idxe))
%     hold on
%     plot(tsim(idxs:idxe), azref(idxs:idxe), 'k')
%     legend('a_z', 'ref [m/s^2]')
%     grid on
% 
%     subplot(2, 2, 4)
%     plot(tsim(idxs:idxe-2), ayaw(idxs:idxe-2))
%     hold on
%     plot(tsim(idxs:idxe), ayawref(idxs:idxe), 'k')
%     legend('a_{\psi}', 'ref [rad/s^2]')
%     grid on
end
