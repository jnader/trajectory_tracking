## Trajectory tracking project.

This project was created in 15/06/2021.

### Documentation
All the documentation is located in MATLAB scripts and Simulink Matlab function blocks.
For a recap, this project simulates trajectory tracking with `CoppeliaSim`.

In a terminal:
- start `launch_sim_poco.sh` script,
- in Matlab, run `startup.m` and `init.m`,
- run `test_trajectory_init_script.m`,
- run `test_trajectory.slx` simulink model and use the joystick `LB` button to initiate a trajectory every time. (to be automated) 

**By default, the drone will come back to the original position (after takeoff) after every trajectory.**

### Task list

- [ ] Save timestamp of beginning of each experiment.
- [ ] Automate all the experiment (no need to click for every trajecotry).
