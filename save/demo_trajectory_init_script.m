% This script is used after `init.m` script in simulation of trajectory
% tracking.y
% You should have launched in a terminal with ID=1
% and launched `roscore` with `CoppeliaSim` and started the simulation.
% In addition, load `waypoints.mat`, open the Simulink model
% `test_trajectory.slx`,launch this script and start simulink model.

if flag_simulationReal == 1
    rosinit;
    chatpub = rospublisher('/vrep/pos1', 'geometry_msgs/Pose');
    msg = rosmessage(chatpub);
end

%%%

nb_trials = 11;
max_traj = nb_trials;


p_nom = [6.5e-4, 0.0154, 0, 0]; % kf, ktau, gx, gy
perturbation = 0.05;
displacement = 0.025;

p_tmp = zeros(nb_trials, length(p_nom));
p_tmp(1, :) = p_nom;

for i=2:nb_trials
    p_tmp = p_nom + [2*(rand(1, 2)-0.5)*perturbation.*p_nom(1:2), 2*(rand(1, 2)-0.5)*displacement];
end

load trajectories/jetson4/11tr_4p.mat
opt_cond = 'IG';
traj_num = 0;


wp = []; wp_t = eval(['traj_' num2str(traj_num) '.' opt_cond '_wp_t']);
for i = 1:length(wp_t)
    wp_tmp = eval(['traj_' num2str(traj_num) '.' opt_cond '_wp']);
    wp = [wp squeeze(wp_tmp(i, :, :))];
end

t_traj = linspace(0, wp_t(end), floor(100*wp_t(end)));
xyzyaw_t = zeros(4, length(t_traj));

for i = 1:length(t_traj)
    xyzyaw_t = [xyzyaw_t waypoints2trajectory(t_traj(i), wp, wp_t)'];
end

Dxyzyaw = max(xyzyaw_t, [], 2) - min(xyzyaw_t, [], 2);
fprintf('\nFor this trajectory : \n\nDx = %f [m],\nDy = %f [m],\nDz = %f [m],\nDyaw = %f [rad].\n\n', Dxyzyaw(1), Dxyzyaw(2), Dxyzyaw(3), Dxyzyaw(4));

Dzmax = 2.15; % maximum altitude variation in m
zmargin = Dzmax - Dxyzyaw(3);
takeoffalt = 0.4 - min(xyzyaw_t(3, :)) + zmargin/2;

if zmargin > 0.1
    fprintf('\nDrone will take off at %f [m] \n\n', takeoffalt);
else
    fprintf('\nz margin is not sufficient...\n\n');
end

%%%

start_robots(robots,gs,log,flag_simulationReal, middleware);
mikroServo_robots(robots);
set_current_position_robots(robots);
set_current_state_robots(robots);
nhfcServo_robots(robots);
% reset_nhfc;
if flag_simulationReal == 1
    forceSimPosition(robots)
end

input('Takeoff now?...')
robots{1}.maneuver.take_off('-a', takeoffalt, 0); % Take off

% Update coppeliasim simulation when drone is taking off.
if flag_simulationReal == 1
    for i = 1:100
        % Read current drone's state.
        current_state = robots{1}.pom.frame('robot');
        current_pos = current_state.frame.pos;
        current_att = current_state.frame.att;
        eul_angles = quat2eul([current_att.qw, current_att.qx, current_att.qy, current_att.qz]);

        % Publish state on `/vrep/pos1` topic.
        msg.Position.X = current_state.frame.pos.x;
        msg.Position.Y = current_state.frame.pos.y;
        msg.Position.Z = current_state.frame.pos.z;

        msg.Orientation.X = current_state.frame.att.qx;
        msg.Orientation.Y = current_state.frame.att.qy;
        msg.Orientation.Z = current_state.frame.att.qz;
        msg.Orientation.W = current_state.frame.att.qw;

        send(chatpub, msg);

        pause(0.05);
    end
else
    pause(5);
end

current_state = robots{1}.pom.frame('robot');
current_pos = current_state.frame.pos;
current_att = current_state.frame.att;
eul_angles = quat2eul([current_att.qw, current_att.qx, current_att.qy, current_att.qz]);
yaw = eul_angles(1);
current_position = [current_pos.x, current_pos.y, current_pos.z, yaw];
wp(1,:) = wp(1,:) + repmat(current_position, 1, length(wp_t));

% Saving origin to use it in `return_to_home` function.
x0 = wp(1,1);
y0 = wp(1,2);
z0 = wp(1,3);
yaw0 = wp(1,4);

if flag_simulationReal == 1
    rosshutdown;
end