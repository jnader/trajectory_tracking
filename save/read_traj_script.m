%% Select and read trajectory

load 11tr_4p.mat
opt_cond = 'IG'; % choose IG, PI, TH or W
traj_num = 0;

target = eval(['traj_' num2str(traj_num) '.' 'TARGET']);

wp = []; wp_t = eval(['traj_' num2str(traj_num) '.' opt_cond '_wp_t']);
T = wp_t(end);

for i = 1:length(wp_t)
    wp_tmp = eval(['traj_' num2str(traj_num) '.' opt_cond '_wp']);
    wp = [wp squeeze(wp_tmp(i, :, :))];
end

