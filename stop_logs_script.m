% This script will stop all the running logs.

for i=1:length(robots)
    robots{i}.pom.log_stop();
    robots{i}.nhfc.log_stop();
    robots{i}.mikro.log_stop();
end